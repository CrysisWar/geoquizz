import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import {LMap, LTileLayer, LMarker, LIcon} from 'vue2-leaflet'
import BootstrapVue from 'bootstrap-vue'

Vue.config.productionTip = false
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-icon', LIcon);
Vue.use(BootstrapVue);

window.axios = axios.create({
  baseURL: 'http://api.player.local:11081/',
  params : {
    
  },
  
});

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});

new Vue({
  router,
  store,
  LMap,
  LTileLayer,
  LMarker,
  LIcon,
  render: h => h(App),
  beforeCreate() {
    this.$store.commit('initialiseStore');
  }
}).$mount('#app')
