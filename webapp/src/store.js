import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token : false,
    nomJoueur: false,
    serie: false, // object : serie -> correspond aux champs de la table serie
    photos: false,// tableau d'objet de photos (la structure correspond à la table photo)
    indice: 0,// Pour savoir à quel stade de la partie on est (photo 1,2,3 ...)
    partie:false,
    score: false,
  
},
mutations: {
    
    setScore(state, score){
        state.score = score;
    },
    setPhoto(state, data) {
        state.photos = data.photos;
        //console.log("Photos :");
        //console.log(data.photos);
    },
    setPartie(state, partie){
        state.partie = partie;
        
    },
    
    setNomJoueur(state, nom_joueur){
        state.nomJoueur = nom_joueur;
    },
    
    setSerie(state, data){
        state.serie = data.serie;
        console.log(data.serie);
    },
    incrementIndice(state){
        state.indice++;
    },

    initialiseStore(state) {
        if(localStorage.getItem('store')) {
            this.replaceState(
                Object.assign(state, JSON.parse(localStorage.getItem('store')))
            );
        }
    }
},
  actions: {

  }
})
