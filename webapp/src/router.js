import Vue from 'vue'
import Router from 'vue-router'
import Bienvenue from './components/Bienvenue.vue'
import Partie from './components/Partie.vue'
import FinPartie from './components/FinPartie.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'bienvenue',
      component: Bienvenue
    },
    {
      path: '/partie',
      name: 'partie',
      component: Partie,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      //component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/FinPartie',
      name: 'finpartie',
      component: FinPartie,
      // route level code
    }
  ]
})
