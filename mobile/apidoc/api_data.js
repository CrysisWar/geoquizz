define({ "api": [
  {
    "type": "put",
    "url": "/geolocalisation/",
    "title": "Recuperation de la description, des coordonnées latitude et longitude d'une photo",
    "name": "receiveGeolocalisation",
    "group": "Geolocalisation",
    "version": "1.0.0",
    "description": "<p>Permet d'obtenir la photo uploadée sur le serveur Cloudordinary ' Retourne la latitude, la longitude et la description, Permet de mettre à jour la BDD avec l'url de Cloudordinary</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>de la photo;</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>de la photo;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>de la photo;</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Une ou plusieurs données sont manquantes dans la requete</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_mobile/index.php",
    "groupTitle": "Geolocalisation"
  },
  {
    "type": "get",
    "url": "/parties/{id}",
    "title": "Récupére les informations d'une partie à titre de test",
    "name": "getParties",
    "group": "Parties",
    "version": "1.0.0",
    "description": "<p>Retourne toutes les informations d'une partie;</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nom_joueur",
            "description": "<p>Nom du joueur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "difficulte",
            "description": "<p>Difficulte de la partie</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_partie",
            "description": "<p>Id de la partie</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token correspondant a l'id de la partie</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "serie",
            "description": "<p>Serie correspondant a l'id_serie fourni en paramètre</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "serie.ville",
            "description": "<p>Nom de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "serie.latitude",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "serie.longitude",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "serie.zoom",
            "description": "<p>Zoom correspondant à la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "photos",
            "description": "<p>Tableau de 10 photos</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photo",
            "description": "<p>Id de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "photos.latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "photos.longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.url",
            "description": "<p>Url de la photo</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 404": [
          {
            "group": "Réponse : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>La partie correspondant à l'id n'a pas été trouvé</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_mobile/index.php",
    "groupTitle": "Parties"
  },
  {
    "type": "post",
    "url": "/photo/",
    "title": "Upload d'une photo sur le serveur",
    "name": "receivePhoto",
    "group": "Photo",
    "version": "1.0.0",
    "description": "<p>Initialisation d'une photo, Envoie d'une photo dans le dossier images Retourne le nom de la photo uploadée</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uploaded",
            "description": "<p>nom de la photo;</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Une ou plusieurs données sont manquantes dans la requete</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_mobile/index.php",
    "groupTitle": "Photo"
  },
  {
    "type": "post",
    "url": "/photos/",
    "title": "Upload de plusieurs photos sur le serveur",
    "name": "receivePhotos",
    "group": "Photos",
    "version": "1.0.0",
    "description": "<p>Initialisation de photos, Envoie de photos dans le dossier images Retourne les noms des photos uploadées</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uploaded",
            "description": "<p>tableau des noms des photos;</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Une ou plusieurs données sont manquantes dans la requete</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_mobile/index.php",
    "groupTitle": "Photos"
  }
] });
