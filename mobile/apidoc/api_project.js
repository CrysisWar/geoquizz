define({
  "name": "mobile",
  "version": "1.0.0",
  "description": "apiDoc mobile",
  "title": "Custom apiDoc browser mobile",
  "url": "https://bitbucket.org/CrysisWar/geoquizz",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-03-20T14:18:10.437Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
