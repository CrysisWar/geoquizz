<?php

namespace gq\backoffice\backjeu\control;

use Firebase\JWT\JWT;
use gq\backoffice\backjeu\error\UnauthorizedError;
use gq\backoffice\backjeu\model\Partie;
use gq\backoffice\backjeu\model\Photo;
use gq\backoffice\backjeu\model\Serie;
use gq\backoffice\backjeu\model\Utilisateur;
use GuzzleHttp\Client;
use gq\backoffice\backjeu\error\MissingDataException;
use gq\backoffice\backjeu\error\NotFound;
use gq\backoffice\backjeu\error\PhpError;
use gq\backoffice\backjeu\error\BadRequest;
use mysql_xdevapi\Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;
use DateTime;


class BackjeuController extends AbstractController {


    public function postAccount(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if(!isset($data['email']) || !isset($data['mdp']))
            return MissingDataException::MissingDataException($request,$response);

        $user = Utilisateur::where('email','=',$data['email'])->first();

        if(empty($user))
            return BadData::error($request,$response);

        if(password_verify($data['mdp'],$user->mdp)) {
            $response = $response->withStatus(200)
                ->withHeader('Content-Type', 'application/json; charset=utf-8');
            return $response;
        } else {
            return BadData::error($request,$response);
        }

    }


    public function postAccountCreate(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if(isset($data['email']) || isset($data['mdp'])) {
            $user = new Utilisateur();
            $uuid4 = Uuid::uuid4();
            $user->id = $uuid4;
            $user->email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
            $user->mdp = password_hash($data['mdp'], PASSWORD_DEFAULT);
            try{
                $user->save();
            } catch (\Exception $e) {
                return PhpError::error($request,$response);
            }
            $response = $response->withStatus(200)
                ->withHeader('Content-Type', 'application/json; charset=utf-8');
            return $response;
        } else {
            return MissingDataException::MissingDataException($request,$response);
        }
    }

    public function getPhotos(Request $request, Response $response, $args)
    {
        try{
            $photos = Photo::select('id','description','latitude','longitude','url')->get();
        }
        catch(\Exception $e){
            return NotFound::error($request,$response);
        }
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($photos));
        return $response;
    }

    public function getOnePhotos(Request $request, Response $response, $args)
    {
        $id = $args['id'];
        if(empty($id))
            return MissingDataException::MissingDataException($request,$response);

        try{
            $photos = Photo::select('id','description','latitude','longitude','url')->where('id','=',$id)->first();
            if(empty($photos))
                return NotFound::error($request,$response);
        }
        catch(\Exception $e){
        return NotFound::error($request,$response);
        }
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($photos));
        return $response;
    }

    public function getPhotosSerie(Request $request, Response $response, $args)
    {
        try {
            $photos = Photo::select('id', 'description', 'latitude', 'longitude', 'url')->where('id_serie', '=', NULL)->get();
            if(empty($photos))
                return NotFound::error($request,$response);
        }
        catch (\Exception $e){
            NotFound::error($request,$response);
        }
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($photos));
        return $response;
    }

    public function putPhoto(Request $request, Response $response, $args)
    {
        $id = $args['id'];
        $data = $request->getParsedBody();
        if(empty($id) || empty($data['description']) || empty($data['id_serie'] || empty($data['latitude']) || empty($data['longitude'])))
            return MissingDataException::MissingDataException($request,$response);

        $photo = Photo::where('id','=',$id)->first();
        if(empty($photo))
            return NotFound::error($request, $response);

        $photo->id_serie = $data['id_serie'];
        $photo->latitude = $data['latitude'];
        $photo->longitude = $data['longitude'];
        $photo->description = $data['description'];
        try{
            $photo->save();
        }
        catch (\Exception $e)
        {
            return PhpError::error($request,$response);
        }
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        return $response;

    }



    public function getSeries(Request $request, Response $response, $args)
    {
        $series = Serie::select('id','ville','latitude','longitude','zoom')->get();
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($series));
        return $response;
    }

    public function postSeries(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if(empty(Serie::where('ville','=',$data['ville'])->get())){
            return NotFound::error($request,$response);
        }
        if(!isset($data['ville']) || !isset($data['latitude']) || !isset($data['longitude']) || !isset($data['zoom'])){
            return MissingDataException::MissingDataException($request, $response);
        }
        $serie = new Serie();
        $serie->ville = $data['ville'];
        $serie->latitude = (float)$data['latitude'];
        $serie->longitude = (float)$data['longitude'];
        $serie->zoom = $data['zoom'];
        try{
            $serie->save();
        }catch (\Exception $e)
        {
            return PhpError::error($request, $response);
        }
        $body = [
            'id' => $serie->id,
            'ville' => $serie->ville,
            'latitude' => (float)$serie['latitude'],
            'longitude' => (float)$serie['longitude'],
            'zoom' => $serie['zoom']
        ];
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($body));
        return $response;
    }

    public function postPhotos(Request $request, Response $response, $args)
    {
        // Todo
        //  upload d'une photo dans le serveur, ajout d'une serie (pas obligé)
    }

    public function putPhotos(Request $request, Response $response, $args)
    {
        // TODO
        //  edition de plusieurs photos, ajout de l'id de la serie
    }




}