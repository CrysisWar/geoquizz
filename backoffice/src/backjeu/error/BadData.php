<?php

namespace gq\backoffice\backjeu\error;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;



class BadData {

    public static function error(Request $rq, Response $rs){
        $data = [
            'type' => "error",
            'error' => 403,
            'message' =>'Une erreur est survenue, l\'identifiant ou le mot de passe est erroné.'
        ];

        $resp = $rs
            ->withHeader('Content-type','application/json;charset=utf-8')
            ->withStatus(403);
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }
}