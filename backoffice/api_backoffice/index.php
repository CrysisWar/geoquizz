<?php

/*
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu Atelier 2 : Réalisation d'une application web
Pauline Monteil : monteilpauline65@gmail.com
Adrien Costa : crywarch@gmail.com
Mike Kaiser : mike.kaiser-parisot8@etu.univ-lorraine.fr
Michael Franiatte : michael.franiatte@gmail.com
*/


/**
 * File:  index.php
 * Creation Date: 15/03/2019
 * name: GéoQuizz / api backoffice
 * description: api pour intéragir avec l'app backoffice
 * type: API POUR APPLI WEB
 *
 * @author: Canals, Monteil, Costa, Kaiser, Franiatte
 */


use gq\backoffice\backjeu\middlewares\CheckTokenMiddleware;
use gq\backoffice\backjeu\control\BackjeuController;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;
use gq\backoffice\backjeu\error\NotAllowed;
use Slim\App;
use Slim\Container;

require __DIR__ . '/../src/vendor/autoload.php';
ini_set('display_errors', 1);

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$c = new Container();      /* Create container */

$conf = [
    'settings' => [
        'displayErrorDetails'=>true,
        'dbconf'=>__DIR__ . '/../src/conf/config.ini',
        'determineRouteBeforeRouteMiddleware'=> true,
        'cors'=>[
            "methods"=> ["GET","POST","PUT","OPTIONS","DELETE"],
            "header.allow" =>['Content-Type','Authorization','X-gq-token'],
            "header.expose"=>[],
            "max.age"=> 60+60,
            "credential"=> true
        ]
    ],
    'notFoundHandler' => function ($c){
        return function (Request $rq, Response $rs){
            return NotAllowed::error($rq,$rs);
        };
    }
];

$app = new App($conf);

$app->options('/{routes:.+}', function (Request $request, Response $response, $args) {
    return $response;
});

$app->add(function (Request $req, Response $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


/**
 * @api {get} /series/ Recupere les informations de toutes les villes
 * @apiName getSeries
 * @apiGroup Serie
 * @apiVersion 1.0.0
 * @apiDescription Retourne tous les id et noms des villes des series présentes dans la BDD
 *
 * @apiSuccess {int} id Id de la ville
 * @apiSuccess {String} ville Nom de la ville
 */
$app->get('/series[/]', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getSeries($request, $response, $args);
})->setName('getSeries');


/**
 * @api {post} /series/ Creation d'une nouvelle série
 * @apiName postSeries
 * @apiGroup Serie
 * @apiVersion 1.0.0
 * @apiDescription Envoie dans la BDD les informations d'une série pour la créer.
 *
 * @apiParam {String} ville Nom de la ville
 * @apiParam {Decimal} latitude Latitude de la ville
 * @apiParam {Decimal} longitude Longitude de la ville
 * @apiParam {int} zoom Valeur du zoom de leaflet
 *
 * @apiSuccess {int} id Id de la ville
 * @apiSuccess {String} ville Nom de la ville
 * @apiSuccess {Decimal} latitude Latitude de la ville
 * @apiSuccess {Decimal} longitude Longitude de la ville
 * @apiSuccess {int} zoom Valeur du zoom de leaflet
 *
 */
$app->post('/series[/]', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->postSeries($request, $response, $args);
});

/**
 * @api {post} /account/ Verification des données d'un compte
 * @apiName postAccount
 * @apiGroup Account
 * @apiVersion 1.0.0
 * @apiDescription Récupère et vérifie les données recues avec celles de la BDD
 *
 * @apiParam {String} email Email de l'utilisateur
 * @apiParam {String} mdp Mot de passe de l'utilisateur
 *
 * @apiSuccess {string} ok
 *
 */
$app->post('/accounts[/]', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->postAccount($request, $response, $args);
})->setName('postAccount');

/**
 * @api {post} /account/create/ Création d'un nouveau compte
 * @apiName postAccountCreate
 * @apiGroup Account
 * @apiVersion 1.0.0
 * @apiDescription Crée un nouveau compte ave cles données recues. Génération d'un UUID en tant qu'id.
 *
 * @apiParam {String} email Email de l'utilisateur
 * @apiParam {String} mdp Mot de passe de l'utilisateur
 *
 * @apiSuccess {OK} OK OK
 *
 */
$app->post('/accounts/create[/]', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->postAccountCreate($request, $response, $args);
})->setName('postAccountCreate');



/**
 * @api {get} /photos/ récupération de toutes les photos
 * @apiName getPhotos
 * @apiGroup Photos
 * @apiVersion 1.0.0
 * @apiDescription récupère toutes les données de toutes les photos stockées dans la base de données.
 *
 * @apiSuccess {Int} id Identifiant de la photo
 * @apiSuccess {String} description Description de la photo
 * @apiSuccess {Decimal} latitude Latitude de la photo
 * @apiSuccess {Decimal} longitude Longitude de la photo
 * @apiSuccess {String} url URL de la photo
 */
$app->get('/photos[/]', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getPhotos($request, $response, $args);
})->setName('getPhotos');


/**
 * @api {get} /photos/series/ récupération de toutes les photos ne possédant pas de série
 * @apiName getPhotosSerie
 * @apiGroup Photos
 * @apiVersion 1.0.0
 * @apiDescription récupère toutes les données de toutes les photos ne possédant pas un id_serie stockées dans la base de données.
 *
 * @apiSuccess {Int} id Identifiant de la photo
 * @apiSuccess {String} description Description de la photo
 * @apiSuccess {Decimal} latitude Latitude de la photo
 * @apiSuccess {Decimal} longitude Longitude de la photo
 * @apiSuccess {String} url URL de la photo
 */
$app->get('/photos/series[/]', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getPhotosSerie($request, $response, $args);
})->setName('getPhotosSerie');


/**
 * @api {get} /photos/{id} récupération des informations d'une photo
 * @apiName getOnePhotos
 * @apiGroup Photos
 * @apiVersion 1.0.0
 * @apiDescription récupère toutes les données d'une photo stockée dans la base de données.
 *
 * @apiParam {int} id Identifiant de la photo
 *
 * @apiSuccess {Int} id Identifiant de la photo
 * @apiSuccess {String} description Description de la photo
 * @apiSuccess {Decimal} latitude Latitude de la photo
 * @apiSuccess {Decimal} longitude Longitude de la photo
 * @apiSuccess {String} url URL de la photo
 */
$app->get('/photos/{id}[/]', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getOnePhotos($request, $response, $args);
})->setName('getOnePhotos');


/**
 * @api {put} /photos/{id} Mise à jour des informations d'une photo
 * @apiName putPhoto
 * @apiGroup Photos
 * @apiVersion 1.0.0
 * @apiDescription mise a jour des informations d'une photo a partir des données fournies.
 *
 * @apiParam {int} id Identifiant de la photo
 * @apiParam {String} description Description de la photo
 * @apiParam {Decimal} latitude Latitude de la photo
 * @apiParam {Decimal} longitude Longitude de la photo
 * @apiParam {int} id_serie Identifiant d'une série
 *
 * @apiSuccess {Int} id Identifiant de la photo
 * @apiSuccess {String} description Description de la photo
 * @apiSuccess {Decimal} latitude Latitude de la photo
 * @apiSuccess {Decimal} longitude Longitude de la photo
 * @apiSuccess {String} url URL de la photo
 */
$app->put('/photos/{id}[/]', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->putPhoto($request, $response, $args);
})->setName('putPhoto');



$app->map(['GET', 'POST', 'PUT'], '/{routes:.+}', function(Request $req, Response $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();
