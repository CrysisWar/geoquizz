define({ "api": [
  {
    "type": "post",
    "url": "/account/",
    "title": "Verification des données d'un compte",
    "name": "postAccount",
    "group": "Account",
    "version": "1.0.0",
    "description": "<p>Récupère et vérifie les données recues avec celles de la BDD</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email de l'utilisateur</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mdp",
            "description": "<p>Mot de passe de l'utilisateur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ok",
            "description": ""
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Account"
  },
  {
    "type": "post",
    "url": "/account/create/",
    "title": "Création d'un nouveau compte",
    "name": "postAccountCreate",
    "group": "Account",
    "version": "1.0.0",
    "description": "<p>Crée un nouveau compte ave cles données recues. Génération d'un UUID en tant qu'id.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email de l'utilisateur</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mdp",
            "description": "<p>Mot de passe de l'utilisateur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "OK",
            "optional": false,
            "field": "OK",
            "description": "<p>OK</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Account"
  },
  {
    "type": "get",
    "url": "/photos/{id}",
    "title": "récupération des informations d'une photo",
    "name": "getOnePhotos",
    "group": "Photos",
    "version": "1.0.0",
    "description": "<p>récupère toutes les données d'une photo stockée dans la base de données.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>URL de la photo</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "get",
    "url": "/photos/",
    "title": "récupération de toutes les photos",
    "name": "getPhotos",
    "group": "Photos",
    "version": "1.0.0",
    "description": "<p>récupère toutes les données de toutes les photos stockées dans la base de données.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>URL de la photo</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "get",
    "url": "/photos/series/",
    "title": "récupération de toutes les photos ne possédant pas de série",
    "name": "getPhotosSerie",
    "group": "Photos",
    "version": "1.0.0",
    "description": "<p>récupère toutes les données de toutes les photos ne possédant pas un id_serie stockées dans la base de données.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>URL de la photo</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "put",
    "url": "/photos/{id}",
    "title": "Mise à jour des informations d'une photo",
    "name": "putPhoto",
    "group": "Photos",
    "version": "1.0.0",
    "description": "<p>mise a jour des informations d'une photo a partir des données fournies.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_serie",
            "description": "<p>Identifiant d'une série</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>URL de la photo</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "get",
    "url": "/series/",
    "title": "Recupere les informations de toutes les villes",
    "name": "getSeries",
    "group": "Serie",
    "version": "1.0.0",
    "description": "<p>Retourne tous les id et noms des villes des series présentes dans la BDD</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Id de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ville",
            "description": "<p>Nom de la ville</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Serie"
  },
  {
    "type": "post",
    "url": "/series/",
    "title": "Creation d'une nouvelle série",
    "name": "postSeries",
    "group": "Serie",
    "version": "1.0.0",
    "description": "<p>Envoie dans la BDD les informations d'une série pour la créer.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ville",
            "description": "<p>Nom de la ville</p>"
          },
          {
            "group": "Parameter",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Parameter",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "zoom",
            "description": "<p>Valeur du zoom de leaflet</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Id de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ville",
            "description": "<p>Nom de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "zoom",
            "description": "<p>Valeur du zoom de leaflet</p>"
          }
        ]
      }
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Serie"
  }
] });
