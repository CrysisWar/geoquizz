<?php

namespace gq\backoffice\backjeu\control;

use gq\backoffice\backjeu\model\Serie;
use gq\backoffice\backjeu\model\Utilisateur;
use \GuzzleHttp\Client;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;
use Slim\Http\UploadedFile;

class BackjeuController extends AbstractController {


    public function index(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        $data = [
            'title' => 'Accueil',
            'current' => 'accueil',
            'session' => $_SESSION
        ];
        return $this->c->view->render($response, 'Backoffice/Home.html', $data);
    }

    public function loginTWIG(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        return $this->c->view->render($response, 'Backoffice/Home.html',
            array(
                'title' => 'Accueil',
                'current' => 'accueil',
                'session' => $_SESSION
            ));
    }

    public function loginAPI(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if (empty($data['email']) || empty($data['mdp'])) {
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('loginTWIG'));
        }
        try {
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('POST', 'accounts',
                array(       'form_params' => [
                    'email' => $data['email'],
                    'mdp' => $data['mdp']]));
        }catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('signin'));
        }
        $_SESSION['email'] = $data['email'];
        return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('index',
            array(
                'title' => 'Accueil',
                'current' => 'accueil',
                'session' => $_SESSION
            )));

    }


    public function logout(Request $request, Response $response, $args){
        unset($_SESSION['email']);
        return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('signin'));
    }


    public function signupTWIG(Request $request, Response $response, $args)
    {
        $data = [
            'title' => 'Ajout Serie',
            'current' => 'series',
            'session' => $_SESSION
        ];
        return $this->c->view->render($response, 'Auth/Create.html', $data);
    }


    public function signupAPI(Request $request,Response $response, $args)
    {
        $data = $request->getParsedBody();
        if(empty($data['email']) || empty($data['mdp'])){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('signup'));
        }
        try{
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('POST','accounts/create',
                [
                    'form_params' => [
                        'email' => $data['email'],
                        'mdp' => $data['mdp']
                    ]
                ]
            );
        }catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('signup'));
        }
        return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('index',
            array(
                'title' => 'Accueil',
                'current' => 'accueil',
                'session' => $_SESSION
            )));


    }


    public function getSeries(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        try{
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('GET','series');
        } catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('addSerie',
                array(
                    'title' => 'Ajout Serie',
                    'current' => 'series',
                    'session' => $_SESSION
                )));
        }
            $data = json_decode($req->getBody());
            return $this->c->view->render($response, 'Backoffice/Serie.html',
                array(
                    'data' => $data,
                    'title' => 'Series',
                    'current' => 'series',
                    'session' => $_SESSION
                ));
    }

    public function addSerie(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        $data = [
            'title' => 'Accueil',
            'current' => 'accueil',
            'session' => $_SESSION
        ];
        return $this->c->view->render($response, 'Backoffice/SerieAdd.html', $data);
    }

    public function postaddSerie(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if (empty($data['ville']) || empty($data['latitude']) || empty($data['longitude'])) {
            $data = [
                'title' => 'Accueil',
                'current' => 'accueil',
                'session' => $_SESSION
            ];
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('addSerie',$data));
        }
        try {
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('POST', 'series',
                [       'form_params' => [
                    'ville' => $data['ville'],
                    'latitude' => $data['latitude'],
                    'longitude' => $data['longitude'],
                    'zoom' => 16
                ]]);
        }catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('series',
                array(
                    'title' => 'Serie',
                    'current' => 'series',
                    'session' => $_SESSION
                )));
        }
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('series',
                array(
                    'data' => $data,
                    'title' => 'Serie',
                    'current' => 'series',
                    'session' => $_SESSION
                )));
    }


    public function getPhotos(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        try{
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 4.0,]);
            $req = $client->request('GET','photos');
        }catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('index',
                array(
                    'title' => 'Accueil',
                    'current' => 'accueil',
                    'session' => $_SESSION
                )));
        }
        $data = json_decode($req->getBody());
        return $this->c->view->render($response, 'Backoffice/Photos.html',
            array(
                'data' => $data,
                'title' => 'Photos',
                'current' => 'photos',
                'session' => $_SESSION
            ));
    }

    public function getPhotosSerie(Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        try{
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 4.0,]);
            $req = $client->request('GET','photos/series');
        } catch(\Exception $e){
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('index',
                array(
                    'title' => 'Accueil',
                    'current' => 'accueil',
                    'session' => $_SESSION
                )));
        }
        $data = json_decode($req->getBody());
        return $this->c->view->render($response, 'Backoffice/PhotoSerie.html',
            array(
                'data' => $data,
                'title' => 'Photos',
                'current' => 'photos',
                'session' => $_SESSION
            ));
    }


    public function modifPhoto(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        try{
            $url = 'photos/'.$data['photo'];
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('GET',$url);
            $req2 = $client->request('GET','series');
        }
        catch(\Exception $e) {
            return $this->c->view->render($response, 'Backoffice/index.html',
                array(
                    'title' => 'Accueil',
                    'current' => 'accueil',
                    'session' => $_SESSION
                ));
        }
        $data = json_decode($req->getBody());
        $serie = json_decode($req2->getBody());
        return $this->c->view->render($response, 'Backoffice/PhotoOne.html',
            array(
                'data' => $data,
                'serie' => $serie,
                'title' => 'Edit Photos',
                'current' => 'photos',
                'session' => $_SESSION
            ));
    }

    public function editPhoto(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();

        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        try{
            $url = 'photos/'.$data['photo'];
            $client = new Client(['base_uri' => 'http://api.backoffice.local:10081/', 'timeout' => 2.0,]);
            $req = $client->request('PUT',$url,
                [
                'form_params' => [
                'description' => $data['description'],
                'longitude' => $data['longitude'],
                'latitude' => $data['latitude'],
                'id_serie' => $data['id_serie']
            ]]);
        }
        catch(\Exception $e) {
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('index',
                array(
                    'title' => 'Accueil',
                    'current' => 'accueil',
                    'session' => $_SESSION
                )));
        }
        return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('photosSerie',
            array(
                'title' => 'Photos',
                'current' => 'photos',
                'session' => $_SESSION
            )));

    }

    public function uploadPage( Request $request, Response $response, $args)
    {
        if(!isset($_SESSION['email'])){
            return $this->c->view->render($response, 'Auth/Authentication.html');
        }
        $data = [
            'title' => 'Importer Photo',
            'current' => 'photos',
            'session' => $_SESSION
        ];
        return $this->c->view->render($response, 'Backoffice/PhotoImport.html', $data);
    }

    public function upload(Request $request, Response $response, $args)
    {

        $directory = $this->c->get('upload_directory');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['newfile'];
        if($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $extension = pathinfo($uploadedFile->getClientFilename(),
                PATHINFO_EXTENSION);

            $uuid4 = Uuid::uuid4();
            $basename = $uuid4;
            $filename = sprintf('%s.%0.8s', $basename, $extension);
            $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

            $filename = $directory . $filename;

            /*
            try{

                $client = new Client(['base_uri' => 'https://api.cloudinary.com/v1_1/crysiswar/', 'timeout' => 4.0,]);
                $req = $client->request('POST','image/upload',
                    [
                        ['file' => '@'.$filename]
                    ]);

                \Cloudinary\Uploader::upload($filename, array("public_id" => $uuid4));

            }catch(\Exception $e){
                echo $e;
            }
            */
            return $response->withStatus(302)->withHeader('Location', $this->c['router']->pathFor('uploadPage'));

        }
    }



}