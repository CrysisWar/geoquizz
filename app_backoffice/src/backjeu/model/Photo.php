<?php
namespace gq\backoffice\backjeu\model;
use \Illuminate\Database\Eloquent\Model;

class Photo extends Model{
    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function partie() {
        return $this->belongsToMany('\gq\backoffice\backjeu\model\Partie', 'ordre','id_partie','id_photo');
    }

}