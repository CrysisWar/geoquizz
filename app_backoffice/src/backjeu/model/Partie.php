<?php
namespace gq\backoffice\backjeu\model;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;


class Partie extends Model{
    protected $table = 'partie';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function serie() {
        return $this->belongsTo('\gq\backoffice\backjeu\model\Serie', 'id_serie');
        }
}