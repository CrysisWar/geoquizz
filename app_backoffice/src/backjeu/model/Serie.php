<?php
namespace gq\backoffice\backjeu\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Serie extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'serie';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function photos(){
        return $this->hasMany('\gq\backoffice\backjeu\model\Photo','id_serie');
    }

    public function parties(){
        return $this->hasMany('\gq\backoffice\backjeu\model\Partie','id_serie');
    }
}