<?php

/* 
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu Atelier 2 : Réalisation d'une application web
Pauline Monteil : monteilpauline65@gmail.com
Adrien Costa : crywarch@gmail.com
Mike Kaiser : mike.kaiser-parisot8@etu.univ-lorraine.fr
Michael Franiatte : michael.franiatte@gmail.com
*/


/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: GéoQuizz / app backoffice
 * description: app pour la création de session de jeu
 * type: APPLICATION ADMINISTRATEUR DE GESTION DES PARTIES
 *
 * @author: Canals, Monteil, Costa, Kaiser, Franiatte
 */

//Configuration du projet

use gq\backoffice\backjeu\control\BackjeuController;
use gq\backoffice\backjeu\error\NotAllowed;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;
use Slim\App;
use Slim\Container;
use Slim\Views\Twig;

require __DIR__ . './src/vendor/autoload.php';


session_start();

$var = parse_ini_file(__DIR__ . './src/conf/config.ini');

$conf = [
    'settings' => [
        'displayErrorDetails' => true ,
        'tmpl_dir' => __DIR__ . './ressources/Templates',
        ],
    'notFoundHandler' => function ($c){
        return function (Request $rq, Response $rs){
            return NotAllowed::error($rq, $rs);
        };
    },
    'view' => function( $c ) {
        return new Twig(
            $c['settings']['tmpl_dir'],
            ['debug' => true, 
            'cache' => false
            ]
        );
    },
    "upload_directory" => './ressources/Assets/images/'
];

/*
 *
\Cloudinary::config(array(
    'cloud_name' => 'crysiswar',
    'api_key' => '931643971328356',
    'api_secret' => '-TNKwguOSB7-4cVlfJO20ZJF7WU'
));
*/

// Create Slim app
$app = new App(new Container($conf));


$app->get('/', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->index($request, $response, $args);
})->setName('index');

$app->get('/index', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->index($request, $response, $args);
});

// GESTION DES COMPTES

$app->get('/signin', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->loginTWIG($request, $response, $args);
})->setName('signin');

$app->post('/signin', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->loginAPI($request, $response, $args);
});

$app->get('/signup', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->signupTWIG($request, $response, $args);
})->setName('signup');

$app->post('/signup', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->signupAPI($request, $response, $args);
});

$app->get('/logout', function (Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->logout($request, $response, $args);
})->setName('logout');


// SERIES

$app->get('/series', function (Request $request, Response $response , $args){
    $c = new BackjeuController($this);
    return $c->getSeries($request, $response, $args);
})->setName('series');

$app->get('/addserie', function (Request $request, Response $response , $args){
    $c = new BackjeuController($this);
    return $c->addSerie($request, $response, $args);
})->setName('addSerie');

$app->post('/addserie', function (Request $request, Response $response , $args){
    $c = new BackjeuController($this);
    return $c->postaddSerie($request, $response, $args);
});



$app->get('/photos', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getPhotos($request,$response,$args);
})->setName('photos');

$app->get('/photoseries', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->getPhotosSerie($request,$response,$args);
})->setName('photosSerie');

$app->post('/editphoto', function (Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->editPhoto($request,$response,$args);
});

$app->post('/photoseries', function (Request $request, Response $response, $args){
   $c = new BackjeuController($this);
   return $c->modifPhoto($request, $response, $args);
});


$app->get('/import', function(Request $request, Response $response, $args){
    $c = new BackjeuController($this);
    return $c->uploadPage($request, $response, $args);
})->setName('uploadPage');

$app->post('/import', function(Request $request, Response $response, $args) {
    $c = new BackjeuController($this);
    return $c->upload($request, $response, $args);
});


// Run app
$app->run();

/*
//http://app.backoffice.local:10081/index.php
*/