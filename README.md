# GeoQuizz
Cloner le projet avec :

SSH :
git clone git@bitbucket.org:CrysisWar/geoquizz.git

ou

HTTPS (remplacer [username] par le nom d'utilisateur du compte) : 
git clone https://[username]@bitbucket.org/CrysisWar/geoquizz.git

Ajouter dans "hosts", les domaines suivant :
127.0.0.1 api.player.local
127.0.0.1 api.backoffice.local
127.0.0.1 api.mobile.local

Dans le dossier "geoquizz", entrer (enlever les commentaires du proxy si il y en a un) :
docker-compose -f docker-compose.yml up

Entrer la commande "composer install" dans :
backoffice/src
mobile/src
player/src

Entrer la commande "npm install" dans le dossier webapp puis "npm run serve"





Accès à l'API Player :
http://api.player.local:11081/

Accès à l'API Backoffice :
http://api.backoffice.local:10081/

Accès à l'API Mobile :
http://api.mobile.local:12081/

Accès à la BDD :
localhost:8080

Serveur: db_jeu_gq
Utilisateur : jeu_gq
Mot de passe : jeu_gq
Base de données : jeu_gq
