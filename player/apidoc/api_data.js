define({ "api": [
  {
    "type": "put",
    "url": "/partie/:id",
    "title": "Declaration de fin de partie",
    "name": "finPartie",
    "group": "Partie",
    "version": "1.0.0",
    "description": "<p>Permet de definir la fin d'une partie en envoyant le score dans la base de données et en changeant le parametre status</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>Score du joueur</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 404": [
          {
            "group": "Réponse : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>La partie correspondant à l'id n'a pas été trouvé</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_player/index.php",
    "groupTitle": "Partie"
  },
  {
    "type": "post",
    "url": "/partie/",
    "title": "Initialisation d'une partie",
    "name": "postPartie",
    "group": "Partie",
    "version": "1.0.0",
    "description": "<p>Initialisation d'une partie, création d'une partie dans la BDD. Retourne les informations de la ville choisie et un ensemble d'informations pour 10 photos</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nom_joueur",
            "description": "<p>Nom du joueur</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id_serie",
            "description": "<p>Id de la serie, correspondant au nom de ville</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "difficulte",
            "description": "<p>Difficulte de la partie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_partie",
            "description": "<p>Id de la partie</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token correspondant a l'id de la partie</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "serie",
            "description": "<p>Serie correspondant a l'id_serie fourni en paramètre</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "serie.ville",
            "description": "<p>Nom de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "serie.latitude",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "serie.longitude",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "serie.zoom",
            "description": "<p>Zoom correspondant à la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "photos",
            "description": "<p>Tableau de 10 photos</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photo",
            "description": "<p>Id de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "photos.latitude",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "photos.longitude",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.url",
            "description": "<p>Url de la photo</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Une ou plusieurs données sont manquantes dans la requete</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      }
    },
    "filename": "api_player/index.php",
    "groupTitle": "Partie"
  },
  {
    "type": "get",
    "url": "/series/",
    "title": "Recupere les informations de toutes les villes",
    "name": "getSeries",
    "group": "Serie",
    "version": "1.0.0",
    "description": "<p>Retourne tous les id et noms de svilles des series présentes dans la BDD</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Id de la ville</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ville",
            "description": "<p>Nom de la ville</p>"
          }
        ]
      }
    },
    "filename": "api_player/index.php",
    "groupTitle": "Serie"
  }
] });
