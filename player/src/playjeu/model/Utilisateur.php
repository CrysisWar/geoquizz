<?php
/**
 * Created by PhpStorm.
 * User: AdriWarch
 * Date: 13/03/2019
 * Time: 16:08
 */
namespace gq\player\playjeu\model;
use \Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model{
    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;
}