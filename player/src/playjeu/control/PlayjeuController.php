<?php

namespace gq\player\playjeu\control;

use Firebase\JWT\JWT;
use gq\player\playjeu\error\UnauthorizedError;
use gq\player\playjeu\model\Partie;
use gq\player\playjeu\model\Photo;
use gq\player\playjeu\model\Serie;
use GuzzleHttp\Client;
use gq\player\playjeu\error\MissingDataException;
use gq\player\playjeu\error\NotFound;
use gq\player\playjeu\error\PhpError;
use gq\player\playjeu\error\BadRequest;
use mysql_xdevapi\Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;
use DateTime;


class PlayjeuController extends AbstractController {


    public function getSeries(Request $request, Response $response, $args)
    {
        $body = Serie::select('id','ville')->get();
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($body));
        return $response;
    }

    public function postPartie(Request $request, Response $response, $arg)
    {
        $data = $request->getParsedBody();
        if(!isset($data['nom_joueur']) || !isset($data['id_serie']) || !isset($data['difficulte']) ){
            return MissingDataException::MissingDataException($request, $response);
        }
        $token = random_bytes(32);
        $token = bin2hex($token);
        $uuid4 = Uuid::uuid4();
        $photos = array();
        for ($i = 1; $i <= 10; $i++) {
            $photo =
                Photo::where('id_serie','=',$data['id_serie'])
                    ->inRandomOrder()
                    ->first();
            array_push($photos,
                array('photo' => $photo['id'],
                    'latitude' => $photo['latitude'],
                    'longitude' => $photo['longitude'],
                    'description' => $photo['description'],
                    'URL' => $photo['url']
                ));
        }
        $serie = Serie::find($data['id_serie']);
        $partie = new Partie();
        $partie->id = $uuid4;
        $partie->nom_joueur = $data['nom_joueur'];
        $partie->token = $token;
        $partie->score = 0;
        $partie->status = 1;
        $partie->id_serie = $data['id_serie'];
        $partie->difficulte = $data['difficulte'];
        try{
            $partie->save();
        }catch (\Exception $e){
            return PhpError::error($request,$response);
        }
        $body = [
            'id_partie' => $uuid4,
            'token' => $token,
            'serie' => $serie,
            'photos' => $photos
        ];
        $response = $response->withStatus(201)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($body));
        return $response;
    }

    public function finPartie(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $id = $args['id'];
        $partie = Partie::find($id);
        if(empty($partie)) {
            return NotFound::error($request, $response);
        }
        $partie->score = $data['score'];
        $partie->status = 3;
        try{
            $partie->save();
        }catch(Exception $e){
            return PhpError::error($request, $response);
        }
        $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        return $response;
    }


}