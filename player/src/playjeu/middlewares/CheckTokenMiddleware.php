<?php

namespace gq\player\playjeu\middlewares;

use gq\player\playjeu\model\Partie;
use gq\player\playjeu\error\MissingDataException;
use gq\player\playjeu\error\NotFound;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CheckTokenMiddleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $token = $request->getQueryParam('token', null);
        if(is_null($token))
        {
            $token= $request->getHeader('X-gq-token');
        }
        if(empty($token)){
            return MissingDataException::MissingDataException($request,$response);
        }
        $id = $request->getAttribute('route')->getArgument('id');
        try{
            Partie::where('id','=',$id)->where('token','=',$token)->firstOrFail();
        }
        catch(ModelNotFoundException $e){
            return NotFound::error($request,$response);
        };
        return $next($request,$response);
    }

}
