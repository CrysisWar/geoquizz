<?php

/*
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu Atelier 2 : Réalisation d'une application web
Pauline Monteil : monteilpauline65@gmail.com
Adrien Costa : crywarch@gmail.com
Mike Kaiser : mike.kaiser-parisot8@etu.univ-lorraine.fr
Michael Franiatte : michael.franiatte@gmail.com
*/


/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: GéoQuizz / api player
 * description: api pour intéragir avec l'app player
 * type: API POUR APPLI WEB
 *
 * @author: Canals, Monteil, Costa, Kaiser, Franiatte
 */


use gq\player\playjeu\middlewares\CheckTokenMiddleware;
use gq\player\playjeu\control\PlayjeuController;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;
use gq\player\playjeu\error\NotAllowed;
use Slim\App;
use Slim\Container;

require __DIR__ . '/../src/vendor/autoload.php';
ini_set('display_errors', 1);

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$c = new Container();      /* Create container */

$conf = [
    'settings' => [
        'displayErrorDetails'=>true,
        'dbconf'=>__DIR__ . '/../src/conf/config.ini',
        'determineRouteBeforeRouteMiddleware'=> true,
        'cors'=>[
            "methods"=> ["GET","POST","PUT","OPTIONS","DELETE"],
            "header.allow" =>['Content-Type','Authorization','X-gq-token'],
            "header.expose"=>[],
            "max.age"=> 60+60,
            "credential"=> true
        ]
    ],
    'notFoundHandler' => function ($c){
        return function (Request $rq, Response $rs){
            return NotAllowed::error($rq,$rs);
        };
    }
];

$app = new App($conf);

$app->options('/{routes:.+}', function (Request $request, Response $response, $args) {
    return $response;
});

$app->add(function (Request $req, Response $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


/**
 * @api {get} /series/ Recupere les informations de toutes les villes
 * @apiName getSeries
 * @apiGroup Serie
 * @apiVersion 1.0.0
 * @apiDescription Retourne tous les id et noms de svilles des series présentes dans la BDD
 *
 * @apiSuccess {int} id Id de la ville
 * @apiSuccess {String} ville Nom de la ville
 */
$app->get('/series[/]', function (Request $req, Response $resp, $args){
    $c = new PlayjeuController($this);
    return $c->getSeries($req, $resp, $args);
})->setName('getSeries');

/**
 * @api {post} /partie/ Initialisation d'une partie
 * @apiName postPartie
 * @apiGroup Partie
 * @apiVersion 1.0.0
 * @apiDescription Initialisation d'une partie, création d'une partie dans la BDD.
 * Retourne les informations de la ville choisie et un ensemble d'informations pour 10 photos
 *
 * @apiParam {String} nom_joueur Nom du joueur
 * @apiParam {int} id_serie Id de la serie, correspondant au nom de ville
 * @apiParam {int} difficulte Difficulte de la partie
 *
 * @apiSuccess {String} id_partie Id de la partie
 * @apiSuccess {String} token Token correspondant a l'id de la partie
 * @apiSuccess {Object} serie Serie correspondant a l'id_serie fourni en paramètre
 * @apiSuccess {String} serie.ville Nom de la ville
 * @apiSuccess {Decimal} serie.latitude Latitude de la ville
 * @apiSuccess {Decimal} serie.longitude Longitude de la ville
 * @apiSuccess {int} serie.zoom Zoom correspondant à la ville
 * @apiSuccess {Object} photos Tableau de 10 photos
 * @apiSuccess {String} photos.photo Id de la photo
 * @apiSuccess {Decimal} photos.latitude Latitude de la photo
 * @apiSuccess {Decimal} photos.longitude Longitude de la photo
 * @apiSuccess {String} photos.description Description de la photo
 * @apiSuccess {String} photos.url Url de la photo
 *
 * @apiError (Réponse : 422) MissingDataException Une ou plusieurs données sont manquantes dans la requete
 * @apiError (Réponse : 500) InternalServerError Une erreur est survenue lors du traitement de la requete
 */
$app->post('/parties[/]', function (Request $req, Response $resp, $args){
  $c = new PlayjeuController($this);
  return $c->postPartie($req, $resp, $args);
})->setName('postPartie');

/**
 * @api {put} /partie/:id Declaration de fin de partie
 * @apiName finPartie
 * @apiGroup Partie
 * @apiVersion 1.0.0
 * @apiDescription Permet de definir la fin d'une partie en envoyant
 * le score dans la base de données et en changeant le parametre status
 *
 * @apiParam {int} score Score du joueur
 *
 * @apiError (Réponse : 404) NotFound La partie correspondant à l'id n'a pas été trouvé
 * @apiError (Réponse : 500) InternalServerError Une erreur est survenue lors du traitement de la requete
 */
$app->put('/parties/{id}[/]', function (Request $req, Response $resp, $args){
    $c = new PlayjeuController($this);
    return $c->finPartie($req, $resp, $args);
})->setName('finPartie')
    ->add(new CheckTokenMiddleware() );

$app->map(['GET', 'POST', 'PUT'], '/{routes:.+}', function(Request $req, Response $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();
